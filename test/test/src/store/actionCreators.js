import {NEW_TOURNAMENT, NEXT_MONTH, PREV_MONTH} from "./actions";


export const createPrevMonth = () => ({type: PREV_MONTH});
export const createNextMonth = () => ({type: NEXT_MONTH});
export const createNewTournament = (payload) => ({type: NEW_TOURNAMENT, payload});