import {PREV_MONTH, NEXT_MONTH, NEW_TOURNAMENT} from "../actions";

const initialState = {
    currentDay: new Date().getDate(),
    currentMonth: new Date().getMonth(),
    currentYear: new Date().getFullYear(),
    tournaments: JSON.parse(localStorage.getItem('tournamentsList')) || {}
};

const calendarReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case PREV_MONTH:
            if (state.currentMonth > 0) {
                return {...state, currentMonth: state.currentMonth - 1};
            } else {
                return {...state, currentMonth: 11, currentYear: state.currentYear - 1};
            }

        case NEXT_MONTH:
            if (state.currentMonth < 11) {
                return {...state, currentMonth: state.currentMonth + 1};
            } else {
                return {...state, currentMonth: 0, currentYear: state.currentYear + 1};
            }

        case NEW_TOURNAMENT:
            const {tournaments} = {...state};
            const {year, month, day, title, description, team1, team2} = payload;
            tournaments[year] = tournaments[year] || {};
            tournaments[year][month-1] = tournaments[year][month-1] || {};
            tournaments[year][month-1][day] = {title, description, team1, team2};
            localStorage.setItem('tournamentsList', JSON.stringify(tournaments));
            return {...state, tournaments: {...tournaments}};

        default:
            return state;
    }
};

export default calendarReducer;