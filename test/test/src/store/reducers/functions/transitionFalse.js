import {TRANSITION_FALSE} from "../../actions";

function transitionFalse() {
    return async function(dispatch) {
        const payload = await setTimeout(() => {
            return false;
        }, 600);
        dispatch({type: TRANSITION_FALSE})
    }
}

export default transitionFalse;