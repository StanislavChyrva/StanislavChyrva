import React from "react";
import styles from "./DayDescription.module.css"
import {shallowEqual, useSelector} from "react-redux";
import logo from "./img/logo.png";
import Button from "../../shared/Button";


const DayDescription = (props) => {
    const {year, month, day, isShow, handleClick} = props;
    const {tournaments} = useSelector(state => state.calendar, shallowEqual);
    const contentYear = tournaments[year] ? tournaments[year] : false;
    const contentMonth = contentYear ? tournaments[year][month] : false;
    const contentDay = contentMonth ? tournaments[year][month][day] : null;


    if (!isShow) {
        return (
            <></>
        )
    }

    if (contentDay) {
        const {title, description, team1, team2} = contentDay;
        return (
            <div className={styles.wrapper} onClick={handleClick}>
                <div className={`${styles.day}`} >
                    <div className={styles.title}>{`${day}.${month+1}.${year}`}</div>

                    <div className={styles.content}>
                        <img src={logo} alt=""/>
                        <div className={styles.contentWrapper}>
                            <h3>{title}</h3>
                            <p className={styles.team}>{`${team1} vs ${team2}`}</p>
                            <p>{description}</p>
                        </div>
                    </div>

                    <div className={styles.buttons}>
                        <Button text="Перейти к турниру" type="flat"/>
                        <Button text="Закрыть" type="flat" handleClick={handleClick}/>
                    </div>

                </div>
            </div>

        )
    }

};

export default DayDescription;