import React, {useEffect, useState} from "react";
import styles from "./Calendar.module.css"
import getCalendarDays from "../../functions/getCalendarDays";
import {shallowEqual, useSelector, useDispatch} from "react-redux";
import {createPrevMonth, createNextMonth} from "../../store/actionCreators";
import nextMonth from "./img/calendar_next_month.svg";
import prevMonth from "./img/calendar_prev_month.svg";
import monthName from "./monthName";
import Button from "../../shared/Button";
import NewTournament from "../NewTournament";


const Calendar = () => {
    const [days, setDays] = useState([]);
    const {currentMonth, currentYear, tournaments} = useSelector(state => state.calendar, shallowEqual);
    const dispatch = useDispatch();

    useEffect(() => {
        setDays(getCalendarDays(currentYear, currentMonth));
    }, [currentYear, currentMonth, tournaments]);

    return (
        <div className={styles.calendar}>
            <div className={styles.title}>
                <h1>{monthName[currentMonth]}, {currentYear} </h1>
                <div className={styles.buttonWrapper}>
                    <button className={styles.nextPrevButtons} onClick={() => {
                        dispatch(createPrevMonth());
                    }
                    }><img
                        src={prevMonth} alt=""/></button>
                    <button className={styles.nextPrevButtons} onClick={() => dispatch(createNextMonth())}><img
                        src={nextMonth} alt=""/></button>
                </div>
            </div>
            <div className={styles.daysTitle}>
                <span>Вс.</span>
                <span>Пн.</span>
                <span>Вт.</span>
                <span>Ср.</span>
                <span>Чт.</span>
                <span>Пт.</span>
                <span>Сб.</span>
            </div>
            <table>
                <tbody>
                {days}
                </tbody>
            </table>

            <div className={styles.buttonContainer}>
                <Button text="Создать турнир" handleClick={() => {}}/>
            </div>

            <NewTournament/>

        </div>
    )
};

export default Calendar