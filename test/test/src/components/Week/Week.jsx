import React from "react";
import styles from "./Week.module.css"

const Week = (props) => {
    const {children} = props;

    return (
        <tr className={styles.week}>
            {children}
        </tr>
    )
};

export default Week