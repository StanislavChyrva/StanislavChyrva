import React, {useState} from "react";
import styles from "./Day.module.css"
import {shallowEqual, useSelector} from "react-redux";
import getPastOrNotClassName from "./getPastOrNotClassName";
import DayDescription from "../DayDescription";
import {CSSTransition} from "react-transition-group";



const Day = (props) => {
    const {year, month, dayOfWeek, dayNumber} = props;
    const {tournaments} = useSelector(state => state.calendar, shallowEqual);
    const [showDescription, setShowDescription] = useState(false);

    const contentYear = tournaments[year] ? tournaments[year] : false;
    const contentMonth = contentYear ? tournaments[year][month] : false;
    const contentDay = contentMonth ? tournaments[year][month][dayNumber] : null;
    const title = contentDay ? contentDay.title : null;
    const teams = contentDay ? `${contentDay.team1} vs ${contentDay.team2}` : null;

    const pastClassName = getPastOrNotClassName(year, month, dayNumber);
    const cursorClassName = contentDay ? styles.pointer : '';

    const handleClick = contentDay ?
        () => {setShowDescription(true)}:
        () => {};

    const closeModal = (event) => {
        if (event.target === event.currentTarget) {
            setShowDescription(false);
        }
    };

    return (
        <>
            <td className={`${styles.day} ${styles[`d${dayOfWeek}`]} ${pastClassName} ${cursorClassName}`} onClick={handleClick}>
                <div className={styles.title}>{dayNumber}</div>
                <div className={styles.content}><h5>{title}</h5> <span>{teams}</span></div>
            </td>
            <CSSTransition in={showDescription} timeout={300} classNames="my-node">
                <DayDescription year={year} month={month} day={dayNumber} isShow={showDescription} handleClick={closeModal}/>
            </CSSTransition>
            </>

    )
};

export default Day;