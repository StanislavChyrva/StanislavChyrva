import styles from "./Day.module.css";

const getPastOrNotClassName = (year, month, day) => {
    const currentDate = new Date();

    let className = null;
    if (year < currentDate.getFullYear()) {
        className = styles.past;
    } else if (year > currentDate.getFullYear()) {
        className = null;
    } else if (month < currentDate.getMonth()) {
        className = styles.past;
    } else if (month > currentDate.getMonth()) {
        className = null;
    } else if (day < currentDate.getDate()) {
        className = styles.past;
    } else if ((day === currentDate.getDate())) {
        className = styles.today;
    } else {
        className = null;
    }

    return className;
};

export default getPastOrNotClassName;