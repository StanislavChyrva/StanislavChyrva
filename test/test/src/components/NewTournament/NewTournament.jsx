import React, {useState} from "react";
import styles from "./NewTournament.module.css"
import Button from "../../shared/Button";
import {useDispatch} from "react-redux";
import {createNewTournament} from "../../store/actionCreators";


const NewTournament = () => {
    const [value, setValue] = useState({
        title: '',
        description: '',
        team1: '',
        team2: '',
        month: '',
        day: ''
    });

    const dispatch = useDispatch();

    const handleChange = ({target}) => {
        const newValue = {...value};
        newValue[target.name] = target.value;
        setValue(newValue);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const payload = {
            year: value.month.split('-')[0],
            month: value.month.split('-')[1],
            day: value.day,
            title: value.title,
            description: value.description,
            team1: value.team1,
            team2: value.team2
        };

        dispatch(createNewTournament(payload));
        setValue({
            title: '',
            description: '',
            team1: '',
            team2: '',
            month: '',
            day: ''
        })

    };

    return (
        <form className={styles.form} onSubmit={handleSubmit}>
            <input type="text" placeholder="Название" onChange={handleChange} name="title"
                   value={value.title}/>
            <input type="text" placeholder="Описание" onChange={handleChange} name="description"
                   value={value.description}/>
            <input type="text" placeholder="Команда" onChange={handleChange} name="team1" value={value.team1}/>
            <input type="text" placeholder="Команда" onChange={handleChange} name="team2" value={value.team2}/>
            <input type="month" placeholder="Month" onChange={handleChange} name="month" value={value.month}/>
            <input type="day" placeholder="День" onChange={handleChange} name="day" value={value.day}/>
            <Button text="Создать" submit={true}/>
        </form>

    )
};

export default NewTournament;