import React from 'react';
import './App.css';
import {Provider} from "react-redux";
import store from "./store/store"
import Calendar from "./components/Calendar";

function App() {




    return (
        <Provider store={store}>
        <div className="App">
         <Calendar/>
        </div>
        </Provider>
    );
}

export default App;
