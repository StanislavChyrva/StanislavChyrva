import Day from "../components/Day";
import Week from "../components/Week";
import React from "react";
import uniqid from "uniqid";

const getCalendarDays = (year, month) => {
    const getDays = (year, month) => {

        const weeks = [];
        for (let i = 0; i < 33; i++) {
            const date = new Date(year, month, 1);
            const nextDate = new Date(date.setDate(date.getDate() + i));
            if (nextDate.getMonth() === month) {
                weeks.push(
                    {
                        year,
                        dayOfWeek: nextDate.getDay(),
                        dayNumber: nextDate.getDate(),
                        month: nextDate.getMonth()
                    });
            }
        }
        return weeks;
    };


    const getMonth = (days) => {
        let monthArray = [];
        const weeksArray = [];
        days.forEach((item, index) => {
            if (item.dayOfWeek !== 0) {
                weeksArray.push(<Day key={`${uniqid('day-')}`} {...item}/>);

            } else if (item.dayOfWeek === 0) {
                monthArray.push(<Week key={`${uniqid('week-')}`}>{[...weeksArray]}</Week>);
                weeksArray.splice(0, weeksArray.length);
                weeksArray.push(<Day key={`${uniqid('day-')}`} {...item}/>);
            }
            if (index === days.length - 1) {
                monthArray.push(<Week key={`${uniqid('week-')}`}>{[...weeksArray]}</Week>);
            }
        });
        return monthArray;
    };

    return getMonth(getDays(year, month));
};

export default getCalendarDays;