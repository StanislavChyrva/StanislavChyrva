import React, {Component} from 'react';
import './layout/css/bootstrap.min.css';
import './App.css';
import {BookForm} from "./components/BookForm";
import {BookList} from "./components/BookList";


class App extends Component {
    state = {
        name: "",
        author: "",
        isbn: "",
        books: [],
    };


    inputChange = ({target}) => {
        this.setState({[target.name]: target.value});
    };

    booksInputChange = ({target}) => {
        const bookId = target.closest('tr').dataset.id; // get id
        const bookIndex = this.state.books.findIndex(({id}) => id === bookId); //get index in array
        const booksArray = [...this.state.books]; // copy books array
        booksArray[bookIndex][target.name] = target.value; //change value of same key:value of input
        this.setState({  //change state
            books: [...booksArray]
        });
    };

    formSubmit = event => {
        event.preventDefault();

        const {name, author, isbn} = this.state;
        const newBook = { //create new book object
            id: `book${(~~(Math.random() * 1e8)).toString(16)}`, //generate random id
            name,
            author,
            isbn,
            changeMode: false // use this param in <BookListItem/> to show/hide edit form
        };

        this.setState(({books}) => {books.push(newBook)}); // add to books array new book
        //zeroing inputs
        this.setState({name: ''});
        this.setState({author: ''});
        this.setState({isbn: ''});

    };

    editBook = (itemId) => {
        const bookIndex = this.state.books.findIndex(({id}) => id === itemId);
        this.setState(({books}) => books[bookIndex].changeMode = true); //show edit from
    };

    deleteBook = (itemId) => {
        const bookIndex = this.state.books.findIndex(({id}) => id === itemId);
        this.setState(({books}) => books.splice(bookIndex, 1)); // delete book object
    };


    updateBook = (itemId) => {
        const bookIndex = this.state.books.findIndex(({id}) => id === itemId);
        this.setState(({books}) => books[bookIndex].changeMode = false); //hide edit form
    };

    render() {
        return (
            <div className="container mt-4">
                <h1 className="display-4 text-center"><i className="fas fa-book-open text-primary"></i> <span
                    className="text-secondary">Book</span> List</h1>
                <p className="text-center">Add your book information to store it in database.</p>


                <div className="row">
                    <div className="col-lg-4">
                        <BookForm inputChange={this.inputChange} formSubmit={this.formSubmit} {...this.state}/>
                    </div>
                </div>

                <BookList {...this.state} editBook={this.editBook} deleteBook={this.deleteBook} updateBook={this.updateBook} booksInputChange={this.booksInputChange}/>
            </div>
        );
    }
}

export default App;
