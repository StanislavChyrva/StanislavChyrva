import React, {Component} from "react";
import {ShowError} from "../ShowError";

export class Field extends Component {
  render(){
  const {name, handleChange, className, type, placeholder, id, value, handleBlur} = this.props;

  return (
        <div className="form-group">
            <label htmlFor={id}>{placeholder}</label>
            <input
                name={name}
                type={type}
                placeholder={placeholder}
                className={className}
                id={id}
                onChange={handleChange}
                value={value}
                required='true'
                onBlur={handleBlur}
            />
        </div>
    );
};
}

export const ValidateField = ShowError(Field);

