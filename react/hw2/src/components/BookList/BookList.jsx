import React, {Component} from "react";
import {BookListItem} from "../BookListItem";


export class BookList extends Component {

    render(){
        const {books, editBook, deleteBook, updateBook, booksInputChange} = this.props;
        const booksElements = books.map(book => <BookListItem {...book} editBook={editBook} deleteBook={deleteBook} updateBook={updateBook} booksInputChange={booksInputChange}/>);
        return (
            <div>
                <h3 id="book-count" className="book-count mt-5">Всего книг: {books.length}</h3>
            <table className="table table-striped mt-2">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>ISBN#</th>
                    <th>Title</th>
                </tr>
                </thead>
                <tbody id="book-list">
                {booksElements}
                </tbody>
            </table>
            </div>
        )
    }
}