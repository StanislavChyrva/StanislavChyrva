import React, {Component} from "react";
import {Field} from "../Field";
import {ValidateField} from "../Field";
import {ShowError} from "../ShowError";

const inputProps = {
    inputTitle: {
        name: 'name',
        type: 'text',
        placeholder: 'Title',
        className: 'form-control',
        id: 'form-control__book-name',
    },
    inputAuthor: {
        name: 'author',
        type: 'text',
        placeholder: 'Author',
        className: 'form-control',
        id: 'form-control__book-author',
    },
    inputISBN: {
        name: 'isbn',
        type: 'text',
        placeholder: 'ISBN#',
        className: 'form-control',
        id: 'form-control__book-isbn',
    }
};

export class BookForm extends Component {


    render(){
        const {inputChange, formSubmit, name, author, isbn} = this.props;
        return (

        <form id="add-book-form" onSubmit={formSubmit}>
            <ValidateField {...inputProps.inputTitle} handleChange={inputChange} value={name}/>
            <ValidateField {...inputProps.inputAuthor} handleChange={inputChange} value={author}/>
            <ValidateField {...inputProps.inputISBN} handleChange={inputChange} value={isbn}/>
            <input type='submit' value="Add Book" className="btn btn-primary" />
        </form>
        )
    }
}