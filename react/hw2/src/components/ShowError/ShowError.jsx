import React, {Component} from "react";
import "./ShowError.css"

export const ShowError = (UserComponent) => {

    return (
        class ShowError extends Component {

            state = {
                error: false
            };

            handleBlur = ({target}) => {
                console.log(target.value.length);
                if(target.value.length === 0) {
                    this.setState({error: true})
                } else {
                    this.setState({error: false})
                }
            };

            render () {
                return(
                    <div>
                        <UserComponent
                            {...this.props}
                            handleBlur={this.handleBlur}
                        />
                        {(this.state.error) ? <p className='input-validate-error'>Enter some data</p> : null}
                    </div>

                )
            }
        }
    )
};
