import React, {Component} from "react";


export class BookListItem extends Component {

    render(){
        const {id, name, author, isbn, editBook, deleteBook, changeMode, updateBook, booksInputChange} = this.props;
        if (changeMode) {
            return (
                    <tr data-id={id}>
                        <td>
                            <div className="form-group">
                                <input type="text" name="name" className="form-control"
                                       value={name} onChange={booksInputChange}/>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <input type="text" name="author" className="form-control" value={author} onChange={booksInputChange}/>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <input type="text" name="isbn" className="form-control" value={isbn} onChange={booksInputChange}/>
                            </div>
                        </td>
                        <td><input type="submit" value="Update" className="btn btn-primary" onClick={() => updateBook(id)}/></td>
                        <td></td>
                    </tr>
        )
        } else {
            return (
                <tr data-id={id}>
                    <td>{name}</td>
                    <td>{author}</td>
                    <td>{isbn}</td>
                    <td><span  className="btn btn-info btn-sm btn-edit" onClick={() => editBook(id)}>EDIT</span></td>
                    <td><span  className="btn btn-danger btn-sm btn-delete" onClick={() => deleteBook(id)}>X</span></td>
                </tr>
            )
        }
    }
}