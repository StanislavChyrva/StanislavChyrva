
import React from "react";
import AppContext from "./AppContext";


const AppProvider = ({value, children}) => {
    return (
        <AppContext.Provider value={value}>
            {children}
        </AppContext.Provider>
        )

};

export default AppProvider;