
const appReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_BOOK':{
            const findItemIndex = state.findIndex(item => item.id === action.book.id);

            if ( findItemIndex === -1){
                const book = {
                    text: action.book.text,
                    id: action.book.id,
                    quantity: 1
                };
                window.localStorage.setItem('shoppingCart', JSON.stringify([...state, book]));
                return [...state, book];

            } else {
                state[findItemIndex].quantity += 1;
                window.localStorage.setItem('shoppingCart', JSON.stringify([...state]));
                return [...state]
            }
        }

        case 'PLUS_BOOK':{
            const findItemIndex = state.findIndex(item => item.id === action.book.id);
            state[findItemIndex].quantity += 1;
            window.localStorage.setItem('shoppingCart', JSON.stringify([...state]));
            return [...state]
        }

        case 'MINUS_BOOK':{
            const findItemIndex = state.findIndex(item => item.id === action.book.id);
            state[findItemIndex].quantity -= 1;
            if ( state[findItemIndex].quantity <= 0) {
                state.splice(findItemIndex, 1);
            }
            window.localStorage.setItem('shoppingCart', JSON.stringify([...state]));
            return [...state]
        }

        case 'DELETE_BOOK':{
            const findItemIndex = state.findIndex(item => item.id === action.book.id);
            state.splice(findItemIndex, 1);
            window.localStorage.setItem('shoppingCart', JSON.stringify([...state]));
            return [...state]
        }

        default:
            throw new Error('Error');
    }
};

export default appReducer;