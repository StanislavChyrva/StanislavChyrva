import React from "react";
import appReducer from "./appReducer";

const AppContext = React.createContext(appReducer, []);

export default AppContext;