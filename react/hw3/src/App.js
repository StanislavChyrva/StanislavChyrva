import React, {useReducer} from 'react';
import './App.css';
import ProductsList from "./clients/components/ProductsList/ProductsList";
import AppProvider from "./context/AppProvider";
import ShoppingCart from "./clients/components/ShopingCart/ShopingCart";
import appReducer from "./context/appReducer"

function App() {
    const initialValue = JSON.parse(window.localStorage.getItem('shoppingCart')) || [];
    const [shoppingCart, setShoppingCart] = useReducer(appReducer, initialValue);
    const value = {shoppingCart, setShoppingCart};

    return (
        <div className="App">
            <AppProvider value={value}>
                <ShoppingCart/>
                <ProductsList/>
            </AppProvider>
        </div>
    );
}

export default App;
