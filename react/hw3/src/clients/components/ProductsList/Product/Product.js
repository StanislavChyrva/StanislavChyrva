import React, {useContext} from "react";
import './Product.css';
import AppContext from "../../../../context/AppContext";
import PropTypes from 'prop-types';

const Product = ({text, id}) => {
    const {setShoppingCart} = useContext(AppContext);

    const addBook = () => {
        setShoppingCart({
                type: "ADD_BOOK",
                book: {text, id}
            })
    };

    return (
        <div className="product" id={id} onClick={addBook}>
            {text}
        </div>
    )
};

export default Product;

Product.propTypes = {
    text: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
};