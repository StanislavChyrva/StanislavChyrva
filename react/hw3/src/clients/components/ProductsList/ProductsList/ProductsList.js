import React from "react";
import Product from "../Product";
import "./ProductsList.css"
const productsParams = [
   'Harry Potter and the Sorcerer’s Stone',
   'Harry Potter and the Chamber of Secrets',
   'Harry Potter and the Prisoner of Azkaban ',
   'Harry Potter and the Goblet of Fire',
   'Harry Potter and the Order of the Phoenix',
   'Harry Potter and the Half-Blood Prince ',
   'Harry Potter and the Deathly Hallows  ',
   'Harry Potter and the Cursed Child'
];
const ProductsList = () => {
    const productsElements = productsParams.map((productText, id) => <Product text={productText} id={id}/>);

    return (
        <div className="products-list">
            {productsElements}
        </div>
    )
};

export default ProductsList;