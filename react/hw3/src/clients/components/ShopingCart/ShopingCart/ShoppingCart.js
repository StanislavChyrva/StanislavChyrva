import React, {useContext, useReducer} from "react";
import './ShoppingCart.css';
import AppContext from "../../../../context/AppContext";
import ShoppingCartItem from "../ShoppingCartItem";

const ShoppingCart = () => {

    const {shoppingCart} = useContext(AppContext);
    const books = shoppingCart.map(item => <ShoppingCartItem {...item}/>);
    return (
        <div className="shopping-cart" >
            <h1>Выбранное</h1>
            {books}
        </div>
    )
};

export default ShoppingCart;