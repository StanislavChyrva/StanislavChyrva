import React, {useContext} from "react";
import './ShoppingCartItem.css';
import Button from "../../../../shared/components/Button";
import AppContext from "../../../../context/AppContext";
import PropTypes from 'prop-types';
import Product from "../../ProductsList/Product";


const ShoppingCartItem = ({text, id, quantity}) => {

    const {setShoppingCart} = useContext(AppContext);

    const plusBook = () => {
        setShoppingCart({
            type: "PLUS_BOOK",
            book: {id}
    })
    };

    const minusBook = () => {
        setShoppingCart({
            type: "MINUS_BOOK",
            book: {id}
        })
    };

    const deleteBook = () => {
        setShoppingCart({
            type: "DELETE_BOOK",
            book: {id}
        })
    };

    return (
        <div className="shopping-cart__item" >
            <span className="shopping-cart__item-text">{text}</span>
            <span className="shopping-cart__item-quantity">{quantity}</span>
            <Button text="+" handleClick={plusBook}/>
            <Button text="-" handleClick={minusBook}/>
            <Button text="Delete" handleClick={deleteBook}/>
        </div>
    )
};

export default ShoppingCartItem;

Product.propTypes = {
    text: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
};