Напишите простейщий функционал корзины на React:

приложение выводит товары в виде списка;

корзина - это пустой (при первом запуске приложения) список товаров с заголовком "Выбранное";

при клике на товар он добавляется в корзину и отображется в в списке "Выбранное";

рядом с каждым товаром в корзине есть кнопки "+" (увеличить количество этого товара в корзине) и "-" (уменьшить его количество);

если количество определенного товара в корзине станет из-за уменьшения равно 0 - он удаляется из корзины;

желательно писать универсальные функции с использованием useReducer;

все операции с корзиной нужно сохранять в localStorage, чтобы при обновлении страницы в корзине отображались выбранные товары;
