import React from 'react';
import './App.css';
import ProductsList from "./clients/componets/ProductList/ProductList";
import Header from "./clients/componets/Header"
import {Provider} from "react-redux";
import store from "./store/store"
import Modal from "./shared/components/Modal";
import CardContainer from "./clients/componets/Card/CardContainer";

function App() {
  return (
      <Provider store={store}>
    <div className="App">
        <Header/>
        <ProductsList/>
        <Modal title="Card">
            <CardContainer/>
        </Modal>
    </div>
      </Provider>
  );
}

export default App;
