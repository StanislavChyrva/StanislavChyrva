import React from "react";
import "./Modal.css";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import Button from "../Button";
import {createHideModal} from "../../../store/actionCreators";

const Modal = (props) => {
    const {title, children} = props;
    const {isShowModal} = useSelector(state => state.modal, shallowEqual);
    const dispatch = useDispatch();
    const modalClassName = isShowModal ? 'modal modal--show' : 'modal';

    return (
        <div className={modalClassName}>
            <div className="modal__background" onClick={() => dispatch(createHideModal())}></div>
            <div className="modal__container">
                <Button text="X" type='close' handleClick={() => dispatch(createHideModal())}/>
                <h3 className='modal__title'>{title}</h3>
            <div className="modal__content-container">
                {children}
            </div>
            </div>
        </div>
    )
};

export default Modal;
