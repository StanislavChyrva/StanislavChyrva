import React from "react";
import "./Button.css";

const Button = (props) => {
    const {text, handleClick, type} = props;

    const buttonStyles = {
        primary: '',
        secondary: 'btn-secondary',
        red: 'btn-red',
        close: 'btn-close',
        item: 'btn-item',
    };

    const buttonClassName = `btn ${buttonStyles[type]}`;
    return (
        <button className={buttonClassName} onClick={handleClick}>{text}</button>
    )
};

export default Button;