import {
    CARD_ADD_ITEM,
    CARD_CLEAR,
    CARD_PLUS_ITEM,
    CARD_MINUS_ITEM,
    CARD_DELETE_ITEM,
    CARD_ONCHANGE_INPUT
} from "../actions";


const initialState = JSON.parse(window.localStorage.getItem('shoppingCart')) || [];

const cardReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case CARD_ADD_ITEM:{
            let findItemIndex = state.findIndex(item => item.id === payload.id);
            if ( findItemIndex === -1){
                const product = {
                    title: payload.title,
                    price: payload.price,
                    id: payload.id,
                    quantity: 1
                };
                window.localStorage.setItem('shoppingCart', JSON.stringify([...state, product]));
                return [...state, product];

            } else {
                const newState = [...state];
                newState[findItemIndex].quantity ++;
                window.localStorage.setItem('shoppingCart', JSON.stringify([...newState]));
                return newState
            }
        }

        case CARD_CLEAR:{
            const newState = [];
            window.localStorage.setItem('shoppingCart', JSON.stringify([...newState]));
            return newState;
        }

        case CARD_PLUS_ITEM: {
            const findItemIndex = state.findIndex(item => item.id === payload.id);
            const newState = [...state];
            newState[findItemIndex].quantity ++;
            window.localStorage.setItem('shoppingCart', JSON.stringify([...newState]));
            return newState
        }

        case CARD_MINUS_ITEM: {
            const findItemIndex = state.findIndex(item => item.id === payload.id);
            const newState = [...state];
            if(newState[findItemIndex].quantity > 1) {
            newState[findItemIndex].quantity --;
            } else {
                newState.splice(findItemIndex, 1);
            }
            window.localStorage.setItem('shoppingCart', JSON.stringify([...newState]));
            return newState
        }

        case CARD_DELETE_ITEM: {
            const findItemIndex = state.findIndex(item => item.id === payload.id);
            const newState = [...state];
            newState.splice(findItemIndex, 1);
            window.localStorage.setItem('shoppingCart', JSON.stringify([...newState]));
            return newState
        }

        case CARD_ONCHANGE_INPUT: {
            const findItemIndex = state.findIndex(item => item.id === payload.id);
            const newState = [...state];
            if (+payload.value < 1) {
                newState.splice(findItemIndex, 1);
            } else {
                newState[findItemIndex].quantity = payload.value;
            }
            window.localStorage.setItem('shoppingCart', JSON.stringify([...newState]));
            return newState
        }



        default:
            return state;
    }
};

export default cardReducer;