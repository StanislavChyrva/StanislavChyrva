import {SHOW_MODAL, HIDE_MODAL} from "../actions";


const initialState = {
    isShowModal: false
};

const modalReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case SHOW_MODAL:
            return {...state, isShowModal: true};
        case HIDE_MODAL:
            return {...state, isShowModal: false};
        default:
            return state;
    }
};

export default modalReducer;