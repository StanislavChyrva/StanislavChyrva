import {combineReducers} from "redux";
import modal from "./modalReducer";
import card from "./cardReducer";


const rootReducer = combineReducers({
    modal,
    card
});

export default rootReducer;