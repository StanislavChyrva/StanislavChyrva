export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';


export const CARD_ADD_ITEM = 'CARD_ADD_ITEM';
export const CARD_CLEAR = 'CARD_CLEAR';
export const CARD_PLUS_ITEM = 'CARD_PLUS_ITEM';
export const CARD_MINUS_ITEM = 'CARD_MINUS_ITEM';
export const CARD_DELETE_ITEM = 'CARD_DELETE_ITEM';
export const CARD_ONCHANGE_INPUT = 'CARD_ONCHANGE_INPUT';

