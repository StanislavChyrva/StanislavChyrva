import {
    SHOW_MODAL,
    HIDE_MODAL,
    CARD_ADD_ITEM,
    CARD_CLEAR,
    CARD_PLUS_ITEM,
    CARD_MINUS_ITEM,
    CARD_DELETE_ITEM,
    CARD_ONCHANGE_INPUT
} from "./actions";

export const createShowModal = () => ({type:SHOW_MODAL});
export const createHideModal = () => ({type:HIDE_MODAL});


export const createCardAddItem = payload => ({type:CARD_ADD_ITEM, payload});
export const createCardCLear = () => ({type:CARD_CLEAR});
export const createCardPlusItem = payload => ({type:CARD_PLUS_ITEM, payload});
export const createCardMinusItem = payload => ({type:CARD_MINUS_ITEM, payload});
export const createCardDeleteItem = payload => ({type:CARD_DELETE_ITEM, payload});
export const createCardOnChangeInput = payload => ({type:CARD_ONCHANGE_INPUT, payload});


