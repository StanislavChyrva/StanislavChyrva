import React from "react";
import "./Header.css"
import Button from "../../../shared/components/Button";
import {useDispatch, useSelector} from "react-redux";
import {createCardCLear, createShowModal} from "../../../store/actionCreators";

const Header = () =>{
    const card = useSelector(state => state.card);
    const dispatch = useDispatch();

    let productsQuantity = card.reduce((a, item) => a + item.quantity, 0);

    return (
        <header className="header">
            <Button text={`Card ${productsQuantity}`} type="primary" handleClick={() => {
                dispatch(createShowModal())}
            }/>
            <Button text="Clear Cart" type="red" handleClick={() => {dispatch(createCardCLear())}}/>
        </header>
    )
};

export default Header;