import React from "react";
import Button from "../../../../shared/components/Button";
import {useDispatch, useSelector} from "react-redux";
import CardItem from "../CardItem";
import "./CardContainer.css"
import {createHideModal} from "../../../../store/actionCreators";

const CardContainer = () => {

    const cardItems = useSelector(state => state.card);
    const dispatch = useDispatch();
    const cardItemsElements = cardItems.length > 0 ? cardItems.map(item => <CardItem {...item}/>) : <h1 className="empty-card">There is nothing here yet ...</h1>;
    const totalPrice = cardItems.reduce((accumulator, item) => accumulator + item.quantity*item.price, 0);
    return (
        <div className="card-container">
          <div className="card-container__items">
              {cardItemsElements}
          </div>
            <div className="card-container__buttons">
                <Button text="Close" type="secondary" handleClick={() => dispatch(createHideModal())}/>
                <Button text="Order Now" type="primary" handleClick={() => {alert("Maybe next time")}}/>
                <span className="card-container__total-price">{`Total price: ${Math.ceil(totalPrice*10)/10}$`}</span>
            </div>


        </div>
    )
};

export default CardContainer;