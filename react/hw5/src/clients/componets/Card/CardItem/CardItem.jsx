import React from "react";
import Button from "../../../../shared/components/Button";
import "./CardItem.css";
import {useDispatch, useSelector} from "react-redux";
import PropTypes from 'prop-types';
import {
    createCardDeleteItem,
    createCardMinusItem,
    createCardOnChangeInput,
    createCardPlusItem
} from "../../../../store/actionCreators";

const CardItem = (props) => {
    const {title, price, id} = props;
    const dispatch = useDispatch();
    const card = useSelector(state => state.card);
    const itemIndex = card.findIndex(item => item.id === id);
    const handleChange = ({target}) => {
        dispatch(createCardOnChangeInput({value: +target.value, id}))
    };
    const totalPrice = Math.ceil((card[itemIndex].quantity * price)*10)/10;


    return (
        <div className="card-item">
            <span className="card-item__name">{title}</span>
            <span className="card-item__price">{price}</span>
            <Button text="-" type='item' handleClick={() => dispatch(createCardMinusItem({id}))}/>
            <input type="number" name={title.toLowerCase()} value={card[itemIndex].quantity} onChange={handleChange}/>
            <Button text="+" type='item' handleClick={() => dispatch(createCardPlusItem({id}))}/>
            <Button text="X" type='close' handleClick={() => dispatch(createCardDeleteItem({id}))}/>
            <span className="card-item__total-price">{`${totalPrice}$`}</span>

        </div>
    )
};

export default CardItem;

CardItem.propTypes = {
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
};