import React from "react";
import './ProductsList.css'

import productsProps from './productsProps/productsProps'
import ProductItem from "../ProductItem";

const ProductsList = (props) => {

    const productsElements = productsProps.map(product => <ProductItem {...product} key={product.title}/>);
    return (
        <div className="products-list">
            {productsElements}
        </div>
    )
};

export default ProductsList;