import orange from "./img/orange.jpg";
import banana from "./img/banana.jpg";
import lemon from "./img/lemon.jpg";

const productsProps = [
    {
        id: '1',
        img: orange,
        alt: 'Nice Orange',
        title: 'Orange',
        price: .5
    },
    {
        id: '2',
        img: banana,
        alt: 'Nice Banana',
        title: 'Banana',
        price: 1.5
    },
    {
        id: '3',
        img: lemon,
        alt: 'Nice Lemon',
        title: 'Lemon',
        price: 2.3
    }
];


export default productsProps;