import React from "react";
import Button from "../../../../shared/components/Button";
import "./ProductItem.css"
import {useDispatch} from "react-redux";
import {createCardAddItem} from "../../../../store/actionCreators";
import PropTypes from "prop-types";

const ProductItem = (props) => {
    const {title, price, img, alt, id} = props;
    const dispatch = useDispatch();

    const addItem = () => {
        dispatch(createCardAddItem({
            title,
            price,
            id
        }))
    };

    return (
        <div className="product-item">
            <div className="product-item__img-container" onClick={addItem}>
                <img src={img} alt={alt}/>
            </div>
            <h3>{title}</h3>
            <span>{`Price: $${price}`}</span>
            <Button text="Add to card" type="primary" handleClick={addItem}/>
        </div>
    )
};

export default ProductItem;

ProductItem.propTypes = {
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    alt: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
};