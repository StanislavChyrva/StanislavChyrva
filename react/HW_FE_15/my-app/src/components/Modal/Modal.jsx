import React from 'react';
import "./Modal.css";
import Button from "../Button";

const Modal = (props) => {
const {isactive, closeModal, header, text, actions, isCloseButton = true, type = 'primary'} = props;


  const modalTypes = { //additional class for modal
    primary: 'modal-primary',
    danger: 'modal-danger',
  };

  const classList = isactive ? `modal ${modalTypes[type]} modal--active` : `modal ${modalTypes[type]}`; //create class string
  const closeButton = isCloseButton ? <Button text="X" type='danger' handleClick={closeModal} /> : null; // create close Section element

  return (
      <div className={classList}>
        <div className="header">
          {header}
          {closeButton}
        </div>

        <div className="content">
          {text}
        </div>

        <div className="buttons-container">
          {actions}
        </div>

        </div>
  );
};

export default Modal;
