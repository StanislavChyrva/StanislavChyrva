import React from 'react';
import "./Button.css";

const Button = (props) => {
const {text, handleCLick, type = 'primary'} = props;

const btnTypes = {
primary: 'btn-primary',
secondary: 'btn-secondary',
danger: 'btn-danger',
};

const classList = `btn ${btnTypes[type]}`;
  return (
  <button className={classList} onClick={handleCLick}>{text}</button>
  );
};

export default Button;
