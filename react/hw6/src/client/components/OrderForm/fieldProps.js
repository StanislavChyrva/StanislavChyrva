const fieldProps = {
    firstName: {
        name: 'firstName',
        id: 'firstName',
        placeholder: 'First name'
    },
    middleName: {
        name: 'middleName',
        id: 'middleName',
        placeholder: 'Middle name'
    },
    lastName: {
        name: 'lastName',
        id: 'lastName',
        placeholder: 'Last name'
    },
    phone: {
        name: 'phone',
        id: 'phone',
        type: 'tel',
        placeholder: '+38 (0__) ___ __ __)'
    },
    email: {
        name: 'email',
        id: 'email',
        type: 'email',
        placeholder: 'Email'
    },
    city: {
        name: 'city',
        value: 'Киев',
        options: [
            {
                text: 'Киев',
                value: 'Киев'
            },
            {
                text: 'Харьков',
                value: 'Харьков'
            },
            {
                text: 'Днепр',
                value: 'Днепр'
            },
            {
                text: 'Одесса',
                value: 'Одесса'
            },
            {
                text: 'Львов',
                value: 'Львов'
            }
        ]
    }
};