import React, {useReducer} from 'react';
import './App.css';
import Stopwatch from "./client/components/Stopwatch/Stopwatch";

function App() {

    return (
    <div className="App">
     <h1>AMAZING AND BRILLIANT REACT STOPWATCH</h1>
      <Stopwatch/>
    </div>
  );
}

export default App;
