import React, {useReducer, useState} from "react";
import Button from "../../../../shared/components/Button";
import stopWatchReducer from "./stopWatchReducer";
import initialValue from "./initialValue";
import "./Stopwatch.css"

/**
 * @desc if value.length = 1 add to string "0". Used in the stopwatch to display a two-digit value
 * @param value {number}
 * @returns {string|*}
 */
const plusNull = (value) => {
    if (value.toString().length === 1) {
        return `0${value}`
    } else {
        return value;
    }
};


const Stopwatch = () => {
    const [stopWatch, setStopWatch] = useReducer(stopWatchReducer, initialValue);
    const [startInterval, setStartInterval] = useState(null); //variable for interval. Used to stop Stopwatch

        const start = () =>{
            setStartInterval( setInterval(() => {
                setStopWatch({
                    type: 'START'
                });
            }, 10))
        };

        const stop = () => {
            clearInterval(startInterval);
        };

        const reset = () => {
            stop();
            setStopWatch({
                type: 'RESET'
            });
        };

        return (
            <div className="stopwatch">
                <h2><span>{plusNull(stopWatch.second)}</span>:<span>{plusNull(stopWatch.milliSecond)}</span></h2>
                <div className="btn-container">
                    <Button text="START" handleClick={start}/>
                    <Button text="STOP" handleClick={stop}/>
                    <Button text="RESET" handleClick={reset}/>
                </div>
            </div>
        )
    };

    export default Stopwatch;