const stopWatchReducer = (state, action) => {
    switch (action.type) {

        case 'START': {
            const newState = {...state};
            if (newState.milliSecond < 99) {
                newState.milliSecond++;
                return newState;
            } else {
                newState.second++;
                newState.milliSecond = 0;
                return newState;
            }
        }
        case 'RESET': {
            return {
                ...state,
                second: 0,
                milliSecond: 0
            }
        }
        default:
            throw new Error('Error');
    }
};

export default stopWatchReducer;