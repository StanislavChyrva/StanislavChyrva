
import React from "react";
import "./index.css"
import {Rating} from "../Rating";
            
export const ContentItem = function(props) {
    const {img, title, category, rating} = props;
    const categoryItems = category.map((item) => {
        //add comma to all element except the last
        let comma = (category.findIndex((elem) => elem === item) !== category.length-1) ? "," : '';
        return <span className="content-item__description-category-span">{item}{comma}</span>
    });


return (
<div className="content-item">
    <div className="content-item__img-wrapper">
        <img src={img} alt=''/>
    </div>
    <div className="content-item__description">
        <h4>{title}</h4>
        <div className="content-item__description-category">{categoryItems}</div>
        <Rating {...rating}/>
    </div>
</div>
)
};          
