
import React from "react";
import "./index.css"
            
export const Preloader = function(props) {
return (
    <div className="preloader">
        <div className="wrapper">
        <div className="preloader-item preloader-item-1"></div>
        <div className="preloader-item preloader-item-2"></div>
        <div className="preloader-item preloader-item-3"></div>
        </div>
        <p className="preloader-description">LOADING</p>

    </div>
)
};          
