
import React from "react";
import "./index.css"
import {TabItem} from "../TabItem";
            
export const TabPanel = function({mainTabs}) {
    const tabItems = mainTabs.map(item => <TabItem {...item}/>);
return (
<div className="tab-panel">
    {tabItems}
</div>
)
};          
