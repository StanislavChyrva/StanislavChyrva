
import React from "react";
import "./index.css"

const buttonType = {
    light: `btn-light`,
    primary: `btn-primary`,
    outline: `btn-outline`,
};

export const Button = function({value, type, href}) {
return (
<a href={href} className={`btn ${buttonType[type]}`}>
    {value}
</a>
)
};          
