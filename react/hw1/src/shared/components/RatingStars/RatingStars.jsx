
import React from "react";
import "./index.css"
import starImg from "../../../assets/images/rating-star.png"
import {Rating} from "../Rating";
            
export const RatingStars = function({rating}) {
    const integerRating = Math.round(rating);
    const starElement = <img src={starImg} className='star-img' alt="rating star"/>
    const starArray = [];
    for (let i = 0; i < integerRating; i++) {
       starArray.push(starElement);
    }
return (
<div className="rating-stars">
    {starArray}
</div>
)
};          
