
import React from "react";
import "./index.css"
            
export const Logo = function({img, alt}) {
return (
<div className="header__logo">
    <img src={img} alt={alt}/>
</div>
)
};          
