
import React from "react";
import "./index.css"
import {Button} from "../Button";
            
export const Banner = function(props) {
    const {img, text, button} = props;
return (
<div className="banner">
    <img src={img} alt=""/>
    <div className="banner__wrapper">
        <p>{text}</p>
        <Button {...button}/>
    </div>

</div>
)
};          
