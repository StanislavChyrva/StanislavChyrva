
import React from "react";
import "./index.css"
            
export const TabItem = function({value, status = ''}) {
return (
<span className={`tab-item tab-item--${status}`}>
    {value}
</span>
)
};          
