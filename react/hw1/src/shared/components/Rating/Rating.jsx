
import React from "react";
import "./index.css"

const ratingTypes = {
    primary: '',
    secondary: 'rating-span--secondary'
};
export const Rating = function({rating, type = 'primary'}) {
return (
// <div className="rating-bar">
<span className={`rating-span ${ratingTypes[type]}`}>{rating}</span>
// </div>
)
};          
