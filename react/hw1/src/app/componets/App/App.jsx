import React from 'react';
import '../../../shared/styles/css/style.css';
import {pageProps} from "./props";
import {Header} from "../../../client/Header/components/Header";
import {Maincontent} from "../../../client/Maincontent/components/Maincontent";
import {Footer} from "../../../client/Footer/components/Footer";

const props = {...pageProps};



export const App = () => {
    return (
        <div className="root">
            <Header {...pageProps}/>
            <Maincontent {...pageProps}/>
            <Footer {...pageProps} />
        </div>

    );
};
