import logoHeader from "../../../assets/images/logo_header.png"
import logoFooter from "../../../assets/images/logo_footer.png"
import contentItem1 from "../../../assets/images/content-item-1.png"
import contentItem2 from "../../../assets/images/content-item-2.png"
import contentItem3 from "../../../assets/images/content-item-3.png"
import contentItem4 from "../../../assets/images/content-item-4.png"

import socialIco1 from "../../../assets/images/social-ico-fb.png"
import socialIco2 from "../../../assets/images/social-ico-twitter.png"
import socialIco3 from "../../../assets/images/social-ico-pinterest.png"
import socialIco4 from "../../../assets/images/social-ico-instagram.png"
import socialIco5 from "../../../assets/images/social-ico-youtube.png"

import banner from "../../../assets/images/banner-img.png"


export const pageProps = {
    logoHeader: {
        img: logoHeader,
        alt: `MOVIERISE Logo`
    },
    buttonHeader: [
        {
            value: 'Sign In',
            type: 'light',
            href: `#1`
        },
        {
            value: 'Sign Up',
            type: 'primary',
            href: `#2`
        }
    ],
    descriptionHeader: {
        title: 'The Jungle Book',
        category: [
            'Adventure',
            'Drama',
            'Family',
            'Fantasy',
        ],
        duration: '1h 46m',
        rating: '4.8',
        buttons: [
            {
                value: 'Watch Now',
                type: 'primary',
                href: `#1`
            },
            {
                value: 'View Info',
                type: 'outline',
                href: `#1`
            },
            {
                value: '+ Favorites',
                type: 'light',
                href: `#1`
            },
        ]
    },
    mainTabs: [
        {
            value: 'Tranding',
            status: 'active'
        }, {
            value: 'Topa Rated',
        }, {
            value: 'New Arrivals',
        }, {
            value: 'Trailers',
        }, {
            value: 'ComingSoon',
        }, {
            value: 'Topa Rated',
        }, {
            value: 'Genre',
        },
    ],
    contentItems: [
        {
            img: contentItem1,
            title: 'ASSASIN’S CREED',
            category: ['Adventure', 'Family', 'Fantasy'],
            rating: {
                rating: '4.8',
                type: 'secondary'
            }
        },
        {
            img: contentItem2,
            title: 'NOW YOU SEE ME 2',
            category: ['Adventure', 'Action', 'Comedy'],
            rating: {
                rating: '4.4',
                type: 'secondary'
            }
        },
        {
            img: contentItem3,
            title: 'CAPTAIN AMERICA',
            category: ['Adventure', 'Action', 'Sci-Fi'],
            rating: {
                rating: '4.2',
                type: 'secondary'
            }
        },
        {
            img: contentItem4,
            title: 'DOCTOR STRANGE',
            category: ['Adventure', 'Family', 'Fantasy'],
            rating: {
                rating: '4.7',
                type: 'secondary'
            }
        },
        {
            img: contentItem1,
            title: 'ASSASIN’S CREED',
            category: ['Adventure', 'Family', 'Fantasy'],
            rating: {
                rating: '4.8',
                type: 'secondary'
            }
        },
        {
            img: contentItem2,
            title: 'NOW YOU SEE ME 2',
            category: ['Adventure', 'Action', 'Comedy'],
            rating: {
                rating: '4.4',
                type: 'secondary'
            }
        },
        {
            img: contentItem3,
            title: 'CAPTAIN AMERICA',
            category: ['Adventure', 'Action', 'Sci-Fi'],
            rating: {
                rating: '4.2',
                type: 'secondary'
            }
        },
        {
            img: contentItem4,
            title: 'DOCTOR STRANGE',
            category: ['Adventure', 'Family', 'Fantasy'],
            rating: {
                rating: '4.7',
                type: 'secondary'
            }
        },
        {
            img: contentItem1,
            title: 'ASSASIN’S CREED',
            category: ['Adventure', 'Family', 'Fantasy'],
            rating: {
                rating: '4.8',
                type: 'secondary'
            }
        },
        {
            img: contentItem2,
            title: 'NOW YOU SEE ME 2',
            category: ['Adventure', 'Action', 'Comedy'],
            rating: {
                rating: '4.4',
                type: 'secondary'
            }
        },
        {
            img: contentItem3,
            title: 'CAPTAIN AMERICA',
            category: ['Adventure', 'Action', 'Sci-Fi'],
            rating: {
                rating: '4.2',
                type: 'secondary'
            }
        },
        {
            img: contentItem4,
            title: 'DOCTOR STRANGE',
            category: ['Adventure', 'Family', 'Fantasy'],
            rating: {
                rating: '4.7',
                type: 'secondary'
            }
        },
    ],
    footer: {
        footerLinks: [
            {
                text: 'About',
                href: '#1'
            }, {
                text: 'Terms Of Service',
                href: '#1'
            },
            {
                text: 'Contacts',
                href: '#2'
            }
        ],
        logoFooter: {
            img: logoFooter,
            alt: `MOVIERISE Logo`
        },
        socialContacts: [
            {
                ico: socialIco1,
                alt: 'Facebook Link',
                href: '#1'
            }, {
                ico: socialIco2,
                alt: 'Twitter Link',
                href: '#1'
            }, {
                ico: socialIco3,
                alt: 'Pinterest Link',
                href: '#1'
            }, {
                ico: socialIco4,
                alt: 'Instagram Link',
                href: '#1'
            }, {
                ico: socialIco5,
                alt: 'YouTube Link',
                href: '#1'
            }
        ],
        copyright: 'Copyright © 2017 MOVIERISE. All Rights Reserved.'
    },

    banner: {
        img: banner,
        text: 'Receive information on the latest hit movies straight to your inbox.',
        button: {
            value: 'Subscribe',
            type: 'primary',
            href: `#2`
        }
    }


};
