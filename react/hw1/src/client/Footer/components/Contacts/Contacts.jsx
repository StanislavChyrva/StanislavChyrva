
import React from "react";
import "./index.css"
            
export const Contacts = function({socialContacts}) {
    const socialContactsElements = socialContacts.map(({href, ico, alt}) => <a className="footer__contacts-link" href={href}><img
        src={ico} alt={alt}/></a>);
return (
<div className="footer__contacts">
    {socialContactsElements}
</div>
)
};          
