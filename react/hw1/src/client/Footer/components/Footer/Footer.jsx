import React from "react";
import "./index.css"
import {Links} from "../Links";
import {Logo} from "../../../../shared/components/Logo";
import {Contacts} from "../Contacts";
import {Copyright} from "../Copyright";

export const Footer = function ({footer}) {
    const {footerLinks, logoFooter, socialContacts, copyright} = footer;
    return (
        <div className="footer">
            <div className="footer__wrapper">
                <Links footerLinks={footerLinks}/>
                <Logo {...logoFooter}/>
                <Contacts socialContacts={socialContacts}/>
            </div>
            <Copyright copyright={copyright}/>
        </div>
    )
};          
