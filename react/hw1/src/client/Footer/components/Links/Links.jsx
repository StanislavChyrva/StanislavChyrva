
import React from "react";
import "./index.css"
            
export const Links = function({footerLinks}) {
    const footerLinksElements = footerLinks.map(({text, href}) => <a className='footer__link' href={href}>{text}</a>);
return (
<div className="footer__links-container">
    {footerLinksElements}
</div>
)
};          
