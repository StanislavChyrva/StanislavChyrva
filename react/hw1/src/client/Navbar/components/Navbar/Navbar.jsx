
import React from "react";
import "./index.css"
import {Logo} from "../../../../shared/components/Logo";
import {Button} from "../../../../shared/components/Button";
import searchIco from "../../../../assets/images/search_icon.png"
            
export const Navbar = function({logo, buttons}) {
    const buttonHeader = buttons.map(button => <Button {...button}/>);
return (
<div className="header__navbar">
    <Logo {...logo}/>
    <div className="header__navbar-buttons">
        <a href="#search"><img src={searchIco} alt="search" className='header__navbar-search-ico'/></a>
        {buttonHeader}
    </div>
</div>
)
};          
