
import React from "react";
import "./index.css"
import {TabPanel} from "../../../../shared/components/TabPanel";
import {ContentContainer} from "../ContentContainer";
import {Preloader} from "../../../../shared/components/Preloader";
import {Banner} from "../../../../shared/components/Banner";

export const Maincontent = function(props) {
    const {mainTabs, contentItems, banner} = props;
return (
<div className="main">
<TabPanel mainTabs={mainTabs} />
<ContentContainer contentItems={contentItems}/>
<Banner {...banner}/>
<ContentContainer contentItems={contentItems.slice(0,4)}/>
<Preloader/>
</div>
)
};          
