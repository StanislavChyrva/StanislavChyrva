
import React from "react";
import "./index.css"
import {ContentItem} from "../../../../shared/components/ContentItem";

export const ContentContainer = function({contentItems}) {
    const contentItemsComponents = contentItems.map(item => <ContentItem {...item} />);
return (
<div className="content-container">
    {contentItemsComponents}
</div>
)
};          
