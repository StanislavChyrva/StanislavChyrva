
import React from "react";
import "./index.css"
import {HeaderFilmDescription} from "../HeaderFilmDescription";
import {Navbar} from "../../../Navbar/components/Navbar";

            
export const Header = function(props) {
    const {logoHeader, buttonHeader, descriptionHeader} = props;
return (
<div className="header">
    <div className="header-wrapper">
        <Navbar logo={logoHeader} buttons={buttonHeader}/>
        <HeaderFilmDescription {...descriptionHeader}/>
    </div>
</div>
)
};          
