
import React from "react";
import "./index.css"
import {RatingStars} from "../../../../shared/components/RatingStars";
import {Rating} from "../../../../shared/components/Rating";
import {Button} from "../../../../shared/components/Button";
import moreIco from "../../../../assets/images/more-ico.png"

export const HeaderFilmDescription = function(props) {
    const {title, category, duration, rating, buttons} = props;
    const categoryElements = category.map(item => <span className="header__category-span">{item}</span>);
    const buttonsElements = buttons.map(item => <Button {...item}/>)
return (
<div className="header__description">

    <div className="header__description-right-section">
        <h2>{title}</h2>
        <div className="header__description-right-section__category-container">
            {categoryElements} |
            <span className="header__category-span header__category-span--margin-left ">{duration}</span>
        </div>

        <div className="header__description-right-section__rating-container">
        <RatingStars rating={rating}/>
        <Rating rating={rating}/>
        </div>
    </div>

    <div className="header__description-left-section">
        {buttonsElements}
        <img src={moreIco} className="more-ico" alt="add more"/>
    </div>

</div>
)
};          
