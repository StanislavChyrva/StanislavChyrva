const fs = require('fs');

const createContent = function(name){
    const content = {
        component: `
import React from "react";
import "./index.css"
            
export const ${name} = function(props) {
return (
<div className="">

</div>
)
};          
`,
        index: `export {${name}} from "./${name}"`
    };

    return content;
};


const createComponent = function (name, path, content) {
    //create component
    fs.mkdirSync(`${path}/${name}`);
    fs.openSync(`${path}/${name}/${name}.jsx`, 'a');
    fs.writeFileSync(`${path}/${name}/${name}.jsx`, content.component);

    //create index.js reexport
    fs.openSync(`${path}/${name}/index.js`, 'a');
    fs.writeFileSync(`${path}/${name}/index.js`, content.index);

    //create index.css
    fs.openSync(`${path}/${name}/index.css`, 'a');
};


module.exports = {
    component: function(fileName) {
        const fileNameUC = fileName[0].toUpperCase() + fileName.slice(1);
        const content = createContent(fileNameUC);
        const path = `./src/shared/components/`;
        createComponent(fileNameUC, path, content);
        return `Component ${fileNameUC} creation successful`;
    },

    module: function(fileName) {
        const fileNameUC = fileName[0].toUpperCase() + fileName.slice(1);
        const content = createContent(fileNameUC);
        fs.mkdirSync(`./src/client/${fileNameUC}`);
        fs.mkdirSync(`./src/client/${fileNameUC}/components`);
        const path = `./src/client/${fileNameUC}/components/`;
        createComponent(fileNameUC, path, content);
        return `Module ${fileNameUC} creation successful`;
    },

    moduleComponent: function(fileName, moduleName) {
        const fileNameUC = fileName[0].toUpperCase() + fileName.slice(1);
        const moduleNameUC = moduleName[0].toUpperCase() + moduleName.slice(1);

        const content = createContent(fileNameUC);
        const path = `./src/client/${moduleNameUC}/components/`;
        createComponent(fileNameUC, path, content);
        return `Component ${fileNameUC} in module ${moduleNameUC} creation successful`;
    }
};


// require('make-runnable');


