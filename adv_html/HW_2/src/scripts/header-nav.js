function headerNavigationButton(){
const navButton = $('.Section');
const navButtonIco = $('.Section i');
const navVerticalMenu = $('header nav');

console.log(navButton);
console.log(navVerticalMenu);
console.log(navButton.attr('data-js-click-identifier'));

$(navButton).on('click', function() {
    if(navButton.attr('data-click-identifier') === '0') {
        $(navVerticalMenu).show('slow');
        $(navButton).attr('data-click-identifier', '1');
        $(navButtonIco).attr('class', 'fas fa-times');

    } else {
        $(navVerticalMenu).hide('slow');
        $(navButton).attr('data-click-identifier', '0');
        $(navButtonIco).attr('class', 'fas fa-bars');

    }

});
}

headerNavigationButton();