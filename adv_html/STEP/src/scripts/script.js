const navButton = $('#headerMenuBurger');
const navButtonIco = $('#headerMenuIco');
const navVerticalMenu = $('#headerMenuUl');

console.log(navButton);
console.log(navVerticalMenu);
console.log(navButtonIco);



$(navButton).on('click', function(event) {
    if(navButton.attr('data-click-identifier') === '0') {
        $(navVerticalMenu).show('slow');
        $(navButton).attr('data-click-identifier', '1');
        $(navButtonIco).attr('class', 'fas fa-times');

    } else {
        $(navVerticalMenu).hide('slow');
        $(navButton).attr('data-click-identifier', '0');
        $(navButtonIco).attr('class', 'fas fa-bars');


    }

});