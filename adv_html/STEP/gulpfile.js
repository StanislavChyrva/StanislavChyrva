const gulp = require('gulp');
const { series, parallel } = require('gulp');
const sass = require('gulp-sass');
const autoPrefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();
const terser = require('gulp-terser');
sass.compiler = require('node-sass');



function html(){
    return gulp.src('src/html/**/*.html')
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());
}

function style() {
    return gulp.src('src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoPrefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
}


function script() {
    return gulp.src('src/scripts/**/*.js')
        .pipe(concat('scripts.min.js'))
        // .pipe(uglify({
        //     toplevel: true
        // }))
        .pipe(terser())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
}

function image() {
    return gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.stream());
}

function cleanHtml() {
    return gulp.src('dist/index.html', {read: false})
        .pipe(clean());
}

function cleanCss() {
    return gulp.src('dist/css', {read: false})
        .pipe(clean());
}


function cleanJs() {
    return gulp.src('dist/js', {read: false})
        .pipe(clean());
}

function cleanImg() {
    return gulp.src('dist/img', {read: false})
        .pipe(clean());
}


function fontawesome () {
    return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(gulp.dest('dist/webfonts/'));
}

function fontawesomeToCss () {
    return gulp.src('src/fontawesome/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('fontawesome.min.css'))
        .pipe(autoPrefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest('dist/fontawesome'))
}

function resetCssToCss () {
    return gulp.src('src/resetCss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('resetCss.min.css'))
        .pipe(autoPrefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest('dist/resetCss/'))
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('src/sass**/*.scss', style);
    gulp.watch('src/scripts/**/*.js', script);
    gulp.watch('src/img/*', image);
    gulp.watch("src/html/*", html).on('change', browserSync.reload);
}


exports.preloader = series(fontawesome, fontawesomeToCss, resetCssToCss);
exports.build = series(parallel(cleanCss, cleanJs, cleanImg, cleanHtml), parallel(style, script, image, html));
exports.dev = series(exports.build, watch);


