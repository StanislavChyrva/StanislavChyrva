let navButton = $('.header__mobile-ico');
let navButtonIco = $('.header__mobile-ico i');
let navVerticalMenu = $('.header-navigation ul')


$(navButton).on('click', function(event) {
    if(navButton.attr('data-click-identifier') === '0') {
        $(navVerticalMenu).show('slow');
        $(navButton).attr('data-click-identifier', '1');
        $(navButtonIco).attr('class', 'fas fa-times');

    } else {
        $(navVerticalMenu).hide('slow');
        $(navButton).attr('data-click-identifier', '0');
        $(navButtonIco).attr('class', 'fas fa-bars');

    }

});