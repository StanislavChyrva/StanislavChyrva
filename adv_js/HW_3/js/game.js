/*
Технические требования:

Создать поле 10*10 с помощью элемента <table>.
Суть игры: любая неподсвеченная ячейка в таблице на короткое время окрашивается в синий цвет. Пользователь должен в течение отведенного времени успеть кликнуть на закрашенную ячейку. Если пользователь успел это сделать, она окрашивается в зеленый цвет, пользователь получает 1 очко. Если не успел - она окрашивается в красный цвет, компьютер получает 1 очко.
Игра длится до тех пор, пока половина ячеек на поле не будут окрашены в зеленый или красный цвет. Как только это случится, тот игрок (человек или компьютер), чьих ячеек на поле больше, побеждает.
Игра должна иметь три уровня сложности, выбираемых перед стартом игры:

Легкий - новая ячейка подсвечивается каждые 1.5 секунды;
Средний - новая ячейка подсвечивается раз в секунду;
Тяжелый - новая ячейка подсвечивается каждые полсекунды.


По окончании игры вывести на экран сообщение о том, кто победил.
После окончания игры должна быть возможность изменить уровень сложности и начать новую игру.
Обязательно использовать функционал ООП при написании приложения.
При желании, вместо окрашивания ячеек цветом, можно вставлять туда картинки.
 */

class newGame {
    constructor(size, mainContainerId, startContainerId, startButtonId) {
        this.size = size;
        this.mainContainerId = mainContainerId;
        this.startButtonId = startButtonId;
        this.startContainerId = startContainerId;
        this.userLevel = null;
        this.level = {
            hard: 500,
            medium: 1000,
            easy: 1500
        };
        this.tableContainer = null;
        this.computerPoints = 0;
        this.userPoints = 0;
        this.interval = null;

    }

    //create array with random numbers
    getRandomArray() {
        const numbersQuantity = this.size * this.size;
        const randomArray = [];

        function createNumber() {
            let randomNumber = Math.floor(Math.random() * numbersQuantity);
            if (randomArray.includes(randomNumber)) {
                if (randomArray.length < numbersQuantity) {
                    createNumber();
                } else {
                    return 'done';
                }
            } else {
                randomArray.push(randomNumber);
                if (randomArray.length < numbersQuantity) {
                    createNumber();
                } else {
                    return 'done';
                }
            }
        }

        createNumber();
        return randomArray;
    }


    createField() {
        const table = document.createElement('table');
        table.setAttribute('class', 'gameContainer');
        const arrayOfHTML = [];
        let idCounter = 0;
        for (let i = 0; i < this.size; i++) {
            let arrayOfTd = [];
            for (let j = 0; j < this.size; j++) {
                let td = `<td id="${idCounter}" data-active="false" data-click="false"></td>`;
                arrayOfTd.push(td);
                idCounter++;
            }
            arrayOfTd.unshift('<tr>');
            arrayOfTd.push('</tr>');
            arrayOfHTML.push(arrayOfTd.join(' '));
        }
        table.innerHTML = arrayOfHTML.join(' ');

        document.getElementById(this.mainContainerId).insertAdjacentHTML('beforeBegin',   `
        <div class="computer-container">
        <h2>AI</h2>
        <span class="computer-counter" id="ai-counter">0</span>
    </div>

    <div class="human-container">
        <h2>YOU</h2>
        <span class="human-counter" id="human-counter">0</span>
    </div>

        `);
        document.getElementById(this.mainContainerId).append(table);
        this.tableContainer = table;
        this.tableContainer.addEventListener('click', this.userClick.bind(this));


    }

    setActiveField() {
        //get random array
        const arrayId = this.getRandomArray();
        //set id counter
        let idCounter = 0;

        this.interval = setInterval(() => {
            if (this.computerPoints >=50 || this.userPoints>=50) {
               this.winFunction();

            }
            if (idCounter < arrayId.length) {
                let activeElement = document.getElementById(`${arrayId[idCounter]}`);
                //set blue background
                activeElement.style.background = 'lightblue';
                activeElement.dataset.active = 'true';
                idCounter++;

                setTimeout(() => {
                    if (activeElement.dataset.click === 'false') {
                        activeElement.style.background = 'lightsalmon';
                        activeElement.dataset.active = 'false';
                        this.computerPoints++;
                        document.getElementsByClassName('computer-counter')[0].innerText = this.computerPoints;

                    }
                }, this.level[this.userLevel] - 10)
            } else {
                return 'done';
            }

        }, this.level[this.userLevel]);
    }

    winFunction(){
        clearInterval(this.interval);

        this.tableContainer.style.opacity = '0';
        document.querySelector('.computer-container').style.opacity = '0';
        document.querySelector('.human-container').style.opacity = '0';
        setTimeout(()=>{
            this.tableContainer.remove();
            document.querySelector('.computer-container').remove();
            document.querySelector('.human-container').remove();


if (this.computerPoints >= this.userPoints) {
    document.getElementById('win-span').innerText = 'COMPUTER WIN';
    console.log('comp');
} else if (this.computerPoints < this.userPoints) {
    console.log('user');

    document.getElementById('win-span').innerText = 'YOU WIN';
}

            document.getElementById('win-message').classList.add('win-animate')
        },1000);

        setTimeout(()=>{
            document.getElementById(this.startContainerId).style.display = '';
            document.getElementById('win-message').classList.remove('win-animate')
            this.userPoints = 0;
            this.computerPoints = 0;
        },3000);





    }

    userClick({target}) {
        if (target.dataset.active === 'true') {
            target.dataset.click = 'true';
            target.style.background = 'lightgreen';
            this.userPoints++;
            document.getElementsByClassName('human-counter')[0].innerText = this.userPoints;
        }
    }

    startGame(e) {
        e.preventDefault();
        this.userLevel = document.querySelector('[type="radio"]:checked').value;
        document.getElementById(this.startContainerId).style.display = 'none';
        this.createField();
        this.setActiveField();

    }


    addListener() {
        document.getElementById(`${this.startButtonId}`).addEventListener('click', this.startGame.bind(this));
    }

    init() {
        this.addListener();
    }

}

let game = new newGame(10, `main-container`, 'pregame-form', 'submit');
game.init();