/*
Создать класс, который позволит имитировать поведение колонки на сервисе Trello.
Класс должен выводить на экран колонку фиксированной ширины.
Колонка должна показывать содержимое в виде карточек.
При первом рендере колонка будет пустая.
В самом низу колонки должна быть кнопка "Добавить карточку". После нажатия на данную кнопку в нижней части колонки перед кнопкой должна появиться новая пустая карточка, в которую можно добавить какой-нибудь текст.
Существующие карточки можно перетягивать вверх и вниз, меняя их порядок в колонке.
В верхней части колонки должна быть кнопка для сортировки карточек в колонке по алфавиту. После сортировки карточки можно свободно перетягивать и менять местами.
Для реализации перетягивания карточек использовать функционал HTML5 Drag&Drop.
Пример финального результата: Пример

В приложении должна быть возможность создать любое количество колонок.
 */

class Card {
    constructor(mainContainerId) {
        this.mainContainerId = mainContainerId;
        this.dragElement = null;
    }

    renderCard() {
        const id = Math.floor(Math.random() * 100);
        const card = document.createElement('div');
        card.setAttribute('class', 'card');
        card.setAttribute('id', `card${id}`);

        const addTaskButton = document.createElement('button');
        addTaskButton.setAttribute('class', 'add-test-Section');
        addTaskButton.setAttribute('id', `button${id}`);
        addTaskButton.innerText = 'Add test';
        addTaskButton.addEventListener('click', this.addTask.bind(this));
        card.append(addTaskButton);

        const sortButton = document.createElement('button');
        sortButton.setAttribute('class', 'sort-Section');
        sortButton.setAttribute('id', `sort-button${id}`);
        sortButton.innerText = 'Sort';
        sortButton.addEventListener('click', this.taskSort.bind(this));
        card.append(sortButton);

        card.addEventListener('dragstart', this.dragStart.bind(this));
        card.addEventListener('drop', this.dragDrop.bind(this));
        card.addEventListener('dragover', this.dragOver.bind(this));
        return card;
    }

    addCard() {
        document.getElementById(this.mainContainerId).append(this.renderCard());
    }

    renderTask() {
        const wrapper = document.createElement('div');
        wrapper.setAttribute('class', 'test-wrapper');
        const newTask = document.createElement('textarea');
        newTask.setAttribute('class', 'test');
        newTask.setAttribute('placeholder', 'Enter name of the test here');
        const okButton = document.createElement('button');
        okButton.setAttribute('class', 'ok-Section');
        okButton.innerText = 'OK';
        okButton.addEventListener('click', this.okButton.bind(this));
        wrapper.append(newTask);
        wrapper.append(okButton);
        return wrapper;

    }

    addTask({target}) {
        const card = target.parentNode;
        card.append(this.renderTask())
    }

    okButton({target}) {
        const cardId = target.parentNode.parentNode.getAttribute('id');
        const taskText = target.parentNode.firstChild.value;
        const taskSpan = document.createElement('span');
        taskSpan.setAttribute('class', 'test-span');
        taskSpan.setAttribute('draggable', 'true');
        taskSpan.setAttribute('data-id', cardId);
        taskSpan.innerText = taskText;
        target.parentNode.parentNode.append(taskSpan);
        target.parentNode.remove();
    }

    dragStart(e) {
        this.dragElement = e.target;
    }

    dragDrop(e) {
        e.preventDefault();
        const cardId = e.currentTarget.getAttribute('id');
        console.log(cardId);
        console.log(e.target);
        console.log(this.dragElement.getAttribute('data-id'));
        if (e.target === e.currentTarget) {
            e.currentTarget.append(this.dragElement);
        } else if (this.dragElement.getAttribute('data-id') === cardId) {
            e.target.before(this.dragElement);
        }
        this.dragElement = null;
    }

    dragOver(e) {
        e.preventDefault();
    }


    taskSort({target}) {
        const cardId = target.parentNode.getAttribute('id');
        const taskCollection = document.querySelectorAll(`#${cardId} span`);
        const taskArray = Array.prototype.slice.call(taskCollection);

        taskArray.sort((a, b) => {
                if (a.innerText > b.innerText) {
                    return 1;
                } else
                if (a.innerText < b.innerText) {
                    return -1;
                } else {
                return 0;
                }
            });

        taskCollection.forEach(elem => elem.remove());
        taskArray.forEach(elem => target.parentNode.append(elem));
    }
}

document.getElementById('add-new-card').addEventListener('click', addCard);

function addCard() {
    const newCard = new Card('main-container');
    newCard.addCard();
}
