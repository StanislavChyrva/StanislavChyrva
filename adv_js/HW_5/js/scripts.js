/*
Технические требования:

Отправить AJAX запрос по адресу https://swapi.dev/api/films/ и получить список всех фильмов серии Звездные войны

Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.
Все запросы на сервер необходимо выполнить с помощью XMLHttpRequest.
 */

const request = new XMLHttpRequest();

request.open('GET', "https://swapi.dev/api/films/");

request.send();

request.onload = function () {
    if (request.status >= 300) { // анализируем HTTP-статус ответа, если статус не 200, то произошла ошибка
        console.log(`Ошибка ${request.status}: ${request.statusText}`); // Например, 404: Not Found
    } else {
        const {results} = JSON.parse(request.response);
        console.log(results);

        for (let i = 0; i < results.length; i++) {
            results[i].characters.forEach(elem => {
                const request1 = new XMLHttpRequest();
                request1.open('GET', elem);
                request1.responseType = 'json';

                request1.send();
                request1.onload = function () {
                    if (request1.status >= 300) { // анализируем HTTP-статус ответа, если статус не 200, то произошла ошибка
                        console.log(`Ошибка ${request1.status}: ${request1.statusText}`); // Например, 404: Not Found
                    } else {
                        document.getElementById(`${i}`).insertAdjacentHTML('beforeEnd', `<li>${ request1.response.name}</li>`);

                    }
                }
            });
        }

        let ulId = 0;
        const resultsHTML = results.map(({title, episode_id, opening_crawl}) => {
            const HTML = `
            <ul >
            <li><strong>Название:</strong> ${title}</li>
            <li><strong>Номер эпизода:</strong> ${episode_id}</li>
            <li><strong>Содержание:</strong> ${opening_crawl}</li>
            <ul id="${ulId}" class="second-ul">
 <strong>Персонажи:</strong>
</ul>
</ul>


            `;
            ulId++;

            return HTML;
        });
        document.getElementById('main-container').insertAdjacentHTML('beforeEnd', resultsHTML.join(' '));
    }
};