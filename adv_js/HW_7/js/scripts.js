/*
Технические требования:

Отправить AJAX запрос по адресу https://swapi.dev/api/films/ и получить список всех фильмов серии Звездные войны

Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.

 */


async function postFilm() {
    const {data} = await axios.get("https://swapi.dev/api/films/");

    let ulId = 0;
    const resultsHTML = data.results.map(({title, episode_id, opening_crawl}) => {
        const HTML = `
            <ul >
            <li><strong>Название:</strong> ${title}</li>
            <li><strong>Номер эпизода:</strong> ${episode_id}</li>
            <li><strong>Содержание:</strong> ${opening_crawl}</li>
            <ul id="${ulId}" class="second-ul">
            <strong>Персонажи:</strong>
            </ul>
            </ul>
            `;
        ulId++;
        return HTML;
    });

    document.getElementById('main-container').insertAdjacentHTML('beforeEnd', resultsHTML.join(' '));


    for (let i = 0; i < data.results.length; i++) {
        const charactersPromiseArray = [];
        data.results[i].characters.forEach(elem => charactersPromiseArray.push(axios.get(elem)));
        const charactersPromise = await Promise.all(charactersPromiseArray);
        charactersPromise.forEach(elem => document.getElementById(`${i}`).insertAdjacentHTML('beforeEnd', `<li>${elem.data.name}</li>`));

    }
}

postFilm();
