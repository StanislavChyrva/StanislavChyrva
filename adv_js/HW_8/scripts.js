
const button = document.body.querySelector('.Section');
const container = document.body.querySelector('.container');

async function getIp(){
    const response = await axios.get('https://api.ipify.org/?format=json');
    const {data} = await axios.get(`http://ip-api.com/json/${response.data.ip}?fields=continent,country,region,city,district&lang=ru`);

    console.log('ip-api.com response: ', data);

    const HTML = `
    <ul>
    <li><strong>Континент:</strong> ${data.continent}</li>
    <li><strong>Страна:</strong> ${data.country}</li>
    <li><strong>Регион:</strong> ${data.region}</li>
    <li><strong>Город:</strong> ${data.city}</li>
    <li><strong>Район:</strong> ${data.district}</li>
</ul>
    `;

    container.innerHTML = HTML;
}


button.addEventListener('click', getIp);