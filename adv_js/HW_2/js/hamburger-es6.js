/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
class Hamburger {
    constructor(size, stuffing) {
        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }


    get size() {
        return this._size;
    }

    set size(value) {
        try {
            if (!value) {
                throw new HamburgerException('Size is not determined')
            }
            if (!(value === Hamburger.SIZE_SMALL || value === Hamburger.SIZE_LARGE)) {
                throw new HamburgerException('Wrong format of size, please insert "Hamburger.SIZE_SMALL" or "Hamburger.SIZE_LARGE"')
            }
            this._size = value;
        } catch (error) {
            console.error(error.name, error.message);
        }
    }


    get stuffing() {
        return this._stuffing;
    }

    set stuffing(value) {
        try {
            if (!value) {
                throw new HamburgerException('Stuffing is not determined')
            }
            if (!(value === Hamburger.STUFFING_CHEESE || value === Hamburger.STUFFING_SALAD || value === Hamburger.STUFFING_POTATO)) {
                throw new HamburgerException('Wrong format of stuffing, please insert "Hamburger.STUFFING_SALAD" or "Hamburger.STUFFING_CHEESE" or "Hamburger.STUFFING_POTATO"')
            }
            this._stuffing = value;
        } catch (error) {
            console.error(error.name, error.message);
        }
    }

    /**
     * Добавить добавку к гамбургеру. Можно добавить несколько
     * добавок, при условии, что они разные.
     *
     * @param topping     Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */
    addTopping(topping) {
        try {
            if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
                throw new HamburgerException('Wrong format of topping, please insert "Hamburger.TOPPING_MAYO" or "Hamburger.TOPPING_SPICE"');
            }
            if (this.topping.includes(topping)) {
                throw new HamburgerException('Duplicate topping');
            }
            this.topping.push(topping);
            console.log('Topping add done (^_^)');
        } catch (error) {
            console.error(error.name, error.message);
        }
    }


    /**
     * Убрать добавку, при условии, что она ранее была
     * добавлена.
     *
     * @param topping   Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */
    removeTopping(topping) {
        try {
            if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
                throw new HamburgerException('Wrong format of topping, please insert "Hamburger.TOPPING_MAYO" or "Hamburger.TOPPING_SPICE"');
            }
            if (!(this.topping.includes(topping))) {
                throw new HamburgerException('No such topping in burger');
            }
            // this.topping.forEach(element => {
            //     if (element === topping) {
            //         this.topping.splice(element, 1)
            //     }
            // })
            this.topping.splice(this.topping.indexOf(topping), 1);
            console.log('Topping removed')
            // )
        } catch (error) {
            console.error(error.name, error.message);

        }
    }


    /**
     * Получить список добавок.
     *
     * @return {Array} Массив добавленных добавок, содержит константы
     *                 Hamburger.TOPPING_*
     */
    getToppings() {
        return this.topping;
    }

    /**
     * Узнать размер гамбургера
     */
    getSize() {
        return this.size;
    }


    /**
     * Узнать начинку гамбургера
     */
    getStuffing() {
        return this.stuffing;
    }

    /**
     * Узнать цену гамбургера
     * @return {Number} Цена в тугриках
     */
    calculatePrice() {
        let hamburgerToppingPrice = 0;
        this.topping.forEach(element => {
            hamburgerToppingPrice += element.price;
        });
        return (this.size.price + this.stuffing.price + hamburgerToppingPrice)
    }


    /**
     * Узнать калорийность
     * @return {Number} Калорийность в калориях
     */
    calculateCalories() {
        let hamburgerToppingKcal = 0;
        this.topping.forEach(element => {
            hamburgerToppingKcal += element.kcal;
        });
        return (this.size.kcal + this.stuffing.kcal + hamburgerToppingKcal)
    }

}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this.name = 'Hamburger Exception:';
    this.message = message;
}


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    name: 'Small',
    price: 50,
    kcal: 20
};
Hamburger.SIZE_LARGE = {
    name: 'Large',
    price: 100,
    kcal: 40
};
Hamburger.STUFFING_CHEESE = {
    name: 'Cheese',
    price: 10,
    kcal: 20
};
Hamburger.STUFFING_SALAD = {
    name: 'Salad',
    price: 20,
    kcal: 5
};
Hamburger.STUFFING_POTATO = {
    name: 'Potato',
    price: 15,
    kcal: 10
};
Hamburger.TOPPING_MAYO = {
    name: 'Mayo',
    price: 20,
    kcal: 5
};
Hamburger.TOPPING_SPICE = {
    name: 'Spice',
    price: 15,
    kcal: 0
};


// const burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
// console.log('burger: ', burger);

const burgerFalse = new Hamburger(Hamburger.SIZE_SMALL); //throw error
const burgerFalse2 = new Hamburger(Hamburger.SIZE_SMALL, 23123); //throw error


const burger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log('burger: ', burger);

burger.addTopping(Hamburger.TOPPING_MAYO);
console.log('burger with topping: ', burger);

burger.removeTopping(Hamburger.TOPPING_SPICE); //throw error

burger.removeTopping(Hamburger.TOPPING_MAYO);
console.log('burger without topping: ', burger);


console.log('stuffing: ', burger.getStuffing());
console.log('size: ', burger.getSize());
console.log('toppings: ', burger.getToppings());
console.log('kcal: ', burger.calculateCalories());
console.log('price: ', burger.calculatePrice());




