/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    try {
        if (!size) {
            throw new HamburgerException('Size is not determined')
        }
        if (!stuffing) {
            throw new HamburgerException('Stuffing is not determined')
        }
        if (!(size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE)) {

            throw new HamburgerException('Wrong format of size, please insert "Hamburger.SIZE_SMALL" or "Hamburger.SIZE_LARGE"')
        }
        if (!(stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO)) {
            throw new HamburgerException('Wrong format of stuffing, please insert "Hamburger.STUFFING_SALAD" or "Hamburger.STUFFING_CHEESE" or "Hamburger.STUFFING_POTATO"')
        }

        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
        console.log(this);

    } catch (error) {
        console.error(error.name, error.message);
    }
}


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    name: 'Small',
    price: 50,
    kcal: 20
};
Hamburger.SIZE_LARGE = {
    name: 'Large',
    price: 100,
    kcal: 40
};
Hamburger.STUFFING_CHEESE = {
    name: 'Cheese',
    price: 10,
    kcal: 20
};
Hamburger.STUFFING_SALAD = {
    name: 'Salad',
    price: 20,
    kcal: 5
};
Hamburger.STUFFING_POTATO = {
    name: 'Potato',
    price: 15,
    kcal: 10
};
Hamburger.TOPPING_MAYO = {
    name: 'Mayo',
    price: 20,
    kcal: 5
};
Hamburger.TOPPING_SPICE = {
    name: 'Spice',
    price: 15,
    kcal: 0
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
            throw new HamburgerException('Wrong format of topping, please insert "Hamburger.TOPPING_MAYO" or "Hamburger.TOPPING_SPICE"');
        }
        if (this.topping.includes(topping)) {
            throw new HamburgerException('Duplicate topping');
        }
        this.topping.push(topping);
        console.log('Topping add done (^_^)');
    } catch (error) {
        console.error(error.name, error.message);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
            throw new HamburgerException('Wrong format of topping, please insert "Hamburger.TOPPING_MAYO" or "Hamburger.TOPPING_SPICE"');
        }
        if (!(this.topping.includes(topping))) {
            throw new HamburgerException('No such topping in burger');
        }
        // this.topping.forEach(element => {
        //     if (element === topping) {
        //         this.topping.splice(element, 1)
        //     }
        // })
        this.topping.splice(this.topping.indexOf(topping), 1);
        console.log('Topping removed')
        // )
    } catch (error) {
        console.error(error.name, error.message);

    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.topping;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {

    var hamburgerToppingPrice = 0;
    this.topping.forEach(element =>{
        hamburgerToppingPrice += element.price;
    });
   return (this.size.price + this.stuffing.price + hamburgerToppingPrice)
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var hamburgerToppingKcal = 0;
    this.topping.forEach(element =>{
        hamburgerToppingKcal += element.kcal;
    });
    return (this.size.kcal + this.stuffing.kcal + hamburgerToppingKcal)
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (message) {
    this.name = 'Hamburger Exception:';
    this.message = message;
}


var burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);




