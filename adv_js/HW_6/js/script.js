/*
Создать страницу, имитирующую ленту новостей социальной сети Twitter.
При открытии страницы, получить с сервера список всех пользователей и общий список публикаций. Для этого нужно отпарвить GET запрос на следующие два адреса:

https://jsonplaceholder.typicode.com/users
https://jsonplaceholder.typicode.com/posts


После загрузки всех пользователей и их публикаций, отобразить все публикации на странице.
Каждая публикация должна быть отображена в виде карточки (пример: https://prnt.sc/q2em0x), и включать заголовок, текст, а также имя, фамилию и имейл пользователя, который ее разместил.
Для каждой карточки должен быть функционал ее редактирования и удаления.
После редактирования карточки необходимо отправить PUT запрос по адресу https://jsonplaceholder.typicode.com/posts/${postId}.
Для удаления карточки необходимо отправить DELETE запрос на https://jsonplaceholder.typicode.com/posts/${postId}.
Вверху страницы должна быть кнопка Добавить публикацию. При нажатии на кнопку, открывать модальное окно, в котором пользователь сможет ввести заголовок и текст публикации. После создания публикации данные о ней необходимо отправить в POST запросе по адресу: https://jsonplaceholder.typicode.com/posts. Новая публикация должна быть добавлена вверху страницы (сортировка в обратном хронологическом порядке). В качестве автора присвоить публикации пользователя с id: 1.
Более детальную информацию по использованию кжадого из этих API можно найти здесь.
Данный сервер (jsonplaceholder.typicode.com) является тестовым. После перезагрузки страницы все изменения, которые отправлялись на сервер, не будут там сохранены. Это нормально.
 */

let usersObject;
let postsObject;

$.ajax({
    url: "https://jsonplaceholder.typicode.com/users", // адрес, на который нужно отправлять запрос
    method: "GET", // тип запроса. Для GET запросов его можно не указывать
    dataType: "json", // тип данных, которые вы ожидаете получить
    success: function (data) { // функция, срабатывающая при успешной отправки и получении ответа. В первый аргумент функции сохранится ответ сервера
        usersObject = data;
        $.ajax({
            url: "https://jsonplaceholder.typicode.com/posts", // адрес, на который нужно отправлять запрос
            method: "GET", // тип запроса. Для GET запросов его можно не указывать
            dataType: "json", // тип данных, которые вы ожидаете получить
            success: function (data) { // функция, срабатывающая при успешной отправки и получении ответа. В первый аргумент функции сохранится ответ сервера
                postsObject = data;
                readyFunction();
            },
            error: showError // функция, срабатывающая при ошибке - например, при отсутствии интернет-соединения
        });
    },
    error: showError // функция, срабатывающая при ошибке - например, при отсутствии интернет-соединения
});


function showError(err) {
    alert(err);
}

function readyFunction() {
    const postHTML = postsObject.map(({title, body, userId, id}) => {
        let postAuthor;
        usersObject.forEach(user => {
            if (user.id === userId) {
                postAuthor = user;
            }
        });

        return `
        <div class="post-container" id="post${id}" data-id="${id}" data-user-id="${userId}">
        <button class="delete">Delete Post</button>
        <button class="change">Change Post</button>
        <h2 class="title">${title}</h2>
        <p class="body">${body}</p>
        <p class="description"> <strong>Author name: </strong> ${postAuthor.name}</p>
        <p class="description"> <strong>Author email: </strong> ${postAuthor.email}</p>   
</div>
        `;

    });

    document.getElementById('main-container').insertAdjacentHTML('beforeEnd', postHTML.join(' '));

    document.querySelectorAll('.delete').forEach(elem => elem.addEventListener('click', postDelete.bind(this)));
    document.querySelectorAll('.change').forEach(elem => elem.addEventListener('click', postChange.bind(this)));


}


function postDelete(e) {
    const postId = e.target.parentNode.getAttribute('data-id');
    $.ajax({
        url: `https://jsonplaceholder.typicode.com/posts/${postId}`,
        method: "DELETE",
        // dataType: "json",
        success: function (data) {
            console.log(data, 'delete');
        },
        error: showError

    });

    e.target.parentNode.remove();

}

function postChange({target}) {

    const title = target.parentNode.querySelector('.title').innerHTML;
    const body = target.parentNode.querySelector('.body').innerHTML;
    console.log(title);
    console.log(body);

    const modalWindow = document.createElement('form');
    modalWindow.setAttribute('class', 'change-modal');
    modalWindow.setAttribute('data-id', `${target.parentNode.dataset.id}`);
    modalWindow.setAttribute('data-user-id', `${target.parentNode.dataset.userId}`);
    modalWindow.innerHTML = `
<label for="change-title"> TITLE</label>
    <textarea id="change-title" name="title">${title}</textarea>
    <label for="change-body" >BODY</label>
    <textarea id="change-body" name="body">${body}</textarea>
    
    `;

    const submitButton = document.createElement('input');
    submitButton.type = 'submit';
    submitButton.value = 'CHANGE';


    modalWindow.addEventListener('submit', submitChange);

    modalWindow.append(submitButton);

    document.body.append(modalWindow);

}

function submitChange(event) {
    event.preventDefault();
    const {currentTarget} = event;
    console.log(currentTarget);

    const body = {
        id: `${currentTarget.dataset.id}`,
        title: `${currentTarget.querySelector('[name="title"]').value}`,
        body: `${currentTarget.querySelector('[name="body"]').value}`,
        userId: `${currentTarget.dataset.userId}`,
    };


    $.ajax({
        url: `https://jsonplaceholder.typicode.com/posts/${currentTarget.dataset.id}`, // адрес, на который нужно отправлять запрос
        method: "PUT", // тип запроса. Для GET запросов его можно не указывать
        dataType: "json", // тип данных, которые вы ожидаете получить
        data: JSON.stringify(body),
        success: function (data) { // функция, срабатывающая при успешной отправки и получении ответа. В первый аргумент функции сохранится ответ сервера
            console.log(data);
            document.body.querySelector(`#post${currentTarget.dataset.id}`).querySelector('.title').innerHTML = body.title;
            document.body.querySelector(`#post${currentTarget.dataset.id}`).querySelector('.body').innerHTML = body.body;
            currentTarget.remove();

        },
        error: showError // функция, срабатывающая при ошибке - например, при отсутствии интернет-соединения
    });


}