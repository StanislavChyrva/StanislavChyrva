
//----------------------------------------------------------------------------------------

function setCookie(name, value, options = {}) {
    options = {
        path: '/',
        ...options
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

// возвращает куки с указанным name,
// или undefined, если ничего не найдено
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}

//---------------------------------------------------------------------------------

const contactUsBtn = document.getElementById('contact-us-btn');
contactUsBtn.addEventListener('click', cookieWrite);

function cookieWrite(){
    setCookie('experiment', 'novalue', {'max-age': 300});

    if(getCookie('new-user')){
        setCookie('new-user', 'false')
    } else {
        setCookie('new-user', 'true')

    }
    console.log(document.cookie);
}